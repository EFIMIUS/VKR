<?php
	require "DataBase.php";
	$data = $_POST;
	$user = R::load('user', $_SESSION['logauth']->id);
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8"/>
		<link rel="stylesheet" type="text/css" href="css/OfficePage.css">
		<link rel="stylesheet" type="text/css" href="css/Fonts.css">
		<link rel="stylesheet" type="text/css" href="css/Header.css">
		<link rel="stylesheet" type="text/css" href="css/Footer.css">
		<title>ЛИЧНЫЙ КАБИНЕТ | JAPAN RIDE CUSTOMS</title>
	</head>
	<body>
		<div class="wrapper">
			<div class="header">
				<div class="navigation">
					<div class="position">
						<a href="/Index.php" class="link_decor"><p class="logo"><span class="jcolor">j</span>rc<span id="domen"><span id="point">&#9632</span>ru</span></p></a>
						<ul class="nav_button">
							<li>новости</li>
							<li>о нас</li>
							<li>услуги</li>
							<li>проекты</li>
							<li>спецпредложения</li>
							<li>контакты</li>
						</ul>
						<?php
							if (isset($_SESSION['logauth'])) 
							{
								echo '<ul class="log_button" id="log_personal_out">
								<li><img src="img/icon/User.png" id="user_position"></li>
								<li><a href="/OfficePage.php" class="link_style">Добро пожаловать, '.$_SESSION['logauth']->name.'</a></li>
								<li><span class="break_line">|</span></li>
								<li><a href="/LogOut.php" class="link_style">Выход</a></li>
								</ul>';
							}else
							{
								echo '<ul class="log_button" id="log_personal_in">
								<li><img src="img/icon/User.png" id="user_position"></li>
								<li><a href="/AuthForm.php" class="link_style">Вход</a></li>
								<li><span class="break_line">|</span></li>
								<li><a href="/RegistForm.php" class="link_style">Регистрация</a></li>
								</ul>';
							}
						?>
					</div>
					<a href="/OrderBuy.php" class="link_style_buy"><div class="orderbuy"><div class="radius_buy"><span id="buy">&#36</span></div></div></a>
				</div>
			</div>
			<div class="content">
				<div class="position">
					<p class="title_page"><span class="title_startsimbol">л</span>ичный кабинет</p>
					<div class="office">
						<div class="office_sidebar">
							<?php
								if(isset($data['save']))
								{	
									$errors = array();
									if ($data['password'] == '')
									{
										$errors[] = 'Введите пароль';
									}
									if(empty($errors))
									{
										$user->name = $data['name'];
										$user->second_name = $data['second_name'];
										$user->phone = $data['phone'];
										$user->email = $data['email'];
										$user->password = password_hash($data['password'], PASSWORD_DEFAULT);
										R::store($user);
										unset($_SESSION['logauth']);
										?><script>window.location.href = "AuthForm.php"</script><?
									}
									else
									{
										echo '<p style="position: absolute;
											font-family: RobotoCondensedBold;
											font-size: 14pt;
											color: #911e1e;
											z-index: 1000;
											top: -26px;">'.array_shift($errors).'</p>';	
									}
								}
							?>
							<form class="personal_data" name="personal" action="/OfficePage.php" method="POST">
								<label class="label_style" id="personal_name_pos">Имя</label>
								<input type="text" name="name" class="input_personal" value="<?php echo $_SESSION['logauth']->name;?>" disabled>
								<label class="label_style" id="personal_second_pos">Фамилия</label>
								<input type="text" name="second_name" class="input_personal" value="<?php echo $_SESSION['logauth']->second_name;?>" disabled>
								<label class="label_style" id="personal_phone_pos">Номер телефона</label>
								<input type="text" name="phone" class="input_personal" value="<?php echo $_SESSION['logauth']->phone;?>" disabled>
								<label class="label_style" id="personal_email_pos">E-mail</label>
								<input type="text" name="email" class="input_personal" value="<?php echo $_SESSION['logauth']->email;?>" disabled>
								<label class="label_style" id="personal_password_pos">Пароль</label>
								<input type="password" name="password" id="password_style" class="input_personal" value="<?php echo $_SESSION['logauth']->password;?>" disabled>
								<button class="click_personal" id="transform_data">изменить данные</button>
								<button class="click_personal" id="save_data" name="save">сохранить данные</button>
							</form>
						</div>
						<div class="office_workwin">
							<table class="table_order">
								<thead>
									<tr>
										<th>#</th>
										<th>Дата<br>заказа</th>
										<th>Наименование<br>услуги</th>
										<th>Марка<br>автомобиля</th>
										<th>Модель<br>автомобиля</th>
										<th>Год<br>выпуска</th>
										<th>Статус<br>выполнеиня</th>
									</tr>
								</thead>
								<tbody class="tbody_skroll">
									<?php
										$list_order = R::find('ordering', 'user_id = ?', array($_SESSION['logauth']->id));
										$status = R::getAll('SELECT id, name FROM status');
										$new_status = array();
										foreach ($status as $one_status)
										{
											$new_status[$one_status['id']] = $one_status;
										}
										foreach ($list_order as $list) 
										{
											echo '<tr>
												<td>'.$list->id.'</td>
												<td>'.$list->date_ord.'</td>
												<td>'.$list->service.'</td>
												<td>'.$list->mark.'</td>
												<td>'.$list->model.'</td>
												<td>'.$list->ago.'</td>
												<td>'.$new_status[$list->status_id]['name'].'</td>
											</tr>';
										}
									?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			<div class="footer">
				<div class="position">
					<div class="footer_menu">
						<div class="fblock_menu">
							<p class="flogo"><span class="jcolor">j</span>rc<span id="fdomen"><span id="fpoint">&#9632</span>ru</span></p>
							<p class="ftext">Вся представленная на сайте информация,<br>
							касающаяся технических характеристик, наличия на<br> 
							складе, стоимости товаров, носит информационный<br> 
							характер и ни при каких условиях не является<br>
							публичной офертой, определяемой положениями<br>
							Статьи 437(2) Гражданского кодекса РФ</p>
						</div>
						<div class="fblock_menu">
							<p class="fsubtext">Контакты</p>
							<ul class="fcontact_menu">
								<li><img src="img/icon/Geo.png" class="icon_position">Ульяновск, Проспект Авиастроителей, 25</li>
								<li><img src="img/icon/Phone.png" class="icon_position">+7(8422)775-05-35</li>
								<li><img src="img/icon/Email.png" class="icon_position"><span id="femail">info@jrc.ru</span></li>
							</ul>
						</div>
						<div class="fblock_menu">
							<p class="fsubtext" id="fsubtext_position">Меню</p>
							<div class="menu_list">
								<ul class="fcontact_menu hover_style ftext_style">
									<li>новости</li>
									<li>о нас</li>
									<li>услуги</li>
								</ul>
								<ul class="fcontact_menu hover_style ftext_style">
									<li>спецпредложения</li>
									<li>проекты</li>
									<li>контакты</li>
								</ul>
							</div>
						</div>
					</div>
					<p class="copyrite">© 2017-2018. <span id="jrc_style">Japan Ride Customs.</span> Японская тачка, может многое, но заряженная японская тачка, может всё!</p>
				</div>
			</div>
		</div>
		<script src="js/jquery.js"></script>
		<script src="js/main.js"></script>
	</body>
</html>