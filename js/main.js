$(document).ready( function() {
	$("#transform_data").click(function(e){
		$("#transform_data").hide();
		$("#save_data").show();
		var f = document.getElementsByTagName("input");
		f.name.disabled = 0;
		f.second_name.disabled = 0;
		f.phone.disabled = 0;
		f.email.disabled = 0;
		f.password.disabled = 0;
		$("#password_style").val("");
		$(".input_personal").css("borderColor", "#911e42");
		e.preventDefault();
	})

	//ВАЛИДАЦИЯ ПОЛЕЙ ОФОРМЛЕНИЯ ЗАКАЗА
	$(".click_order").click( function (e){
		var mark_avto = $("#mark_avto").val();
		var model_avto = $("#model_avto").val();
		var ago_avto = $("#ago_avto").val();
		var service = $("#service").val();
		var dop_info = $("#dop_info").val();
		var error = false;
		if (mark_avto == "")
		{
			$("#mark_avto").css("borderColor", "#911e1e");
			$(".errors_order").show();
			error = true;
		}
		if (model_avto == "")
		{
			$("#model_avto").css("borderColor", "#911e1e");
			$(".errors_order").show();
			error = true;
		} 
		if (ago_avto == "")
		{
			$("#ago_avto").css("borderColor", "#911e1e");
			$(".errors_order").show();
			error = true;
		} 
		if (service == "")
		{
			$("#service").css("borderColor", "#911e1e");
			$(".errors_order").show();
			error = true;
		}
		if (dop_info == "")
		{
			$("#dop_info").css("borderColor", "#911e1e");
			$(".errors_order").show();
			error = true;
		}
		if (!error) 
		{
			$.ajax({
                method : 'POST',
                data : data,
                url : '/OrderBuy.php',             
            });
		}
		e.preventDefault();
	});

	//ФИЛЬТРАЦИЯ ДАННЫХ СЕЛЕКТОРА
	$("#mark_avto").change( function (){
		if ($("#mark_avto").val()=="")
		{
			$(".toyota_model").hide();
			$(".honda_model").hide();
			$(".nissan_model").hide();
			$(".ago_avto").hide();
			$(".service").hide();
			$("#model_avto").val("");
			$("#ago_avto").val("");
			$("#service").val("");
		}
		else if ($("#mark_avto").val()=="Toyota")
		{
			$(".toyota_model").show();
			$(".honda_model").hide();
			$(".nissan_model").hide();
			$(".ago_avto").show();
			$(".service").show();
			$("#model_avto").val("");
			$("#ago_avto").val("");
			$("#service").val("");
		}
		else if ($("#mark_avto").val()=="Honda")
		{
			$(".toyota_model").hide();
			$(".honda_model").show();
			$(".nissan_model").hide();
			$(".ago_avto").show();
			$(".service").show();
			$("#model_avto").val("");
			$("#ago_avto").val("");
			$("#service").val("");
		}
		else if ($("#mark_avto").val()=="Nissan")
		{
			$(".toyota_model").hide();
			$(".honda_model").hide();
			$(".nissan_model").show();
			$(".ago_avto").show();
			$(".service").show();
			$("#model_avto").val("");
			$("#ago_avto").val("");
			$("#service").val("");
		}
	});

	//ИЗМЕНЕНИЕ СТАТУСА ВЫПОЛННЕИЯ РАБОТ
	$(".status_active").change( function (){
		$.ajax({
			method: 'POST',
			url: '/ajax/SelectStatus.php',
			data: {
				order_id: $(this).data('order-id'),
				status_id: $(this).val()
			}
		});
	});

	//УДАЛЕНИЕ ПОЛЬЗОВАТЕЛЯ
	$(".active_delete").click( function(e){
		$.ajax({
			method: 'POST',
			url: '/ajax/UserDelete.php',
			data: {
				user_id: $(this).data('user-id'),
			},
			success: function() {
				location.reload();
			}          
		});
		e.preventDefault();
	});
})
