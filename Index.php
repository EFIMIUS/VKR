<?php
	require "DataBase.php";
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8"/>
		<link rel="stylesheet" type="text/css" href="css/MainPage.css">
		<link rel="stylesheet" type="text/css" href="css/Fonts.css">
		<link rel="stylesheet" type="text/css" href="css/Header.css">
		<link rel="stylesheet" type="text/css" href="css/Footer.css">
		<title>JAPAN RIDE CUSTOMS</title>
	</head>
	<body>
		<div class="wrapper">
			<div class="header">
				<div class="navigation">
					<div class="position">
						<a href="/Index.php" class="link_decor"><p class="logo"><span class="jcolor">j</span>rc<span id="domen"><span id="point">&#9632</span>ru</span></p></a>
						<ul class="nav_button">
							<li>новости</li>
							<li>о нас</li>
							<li>услуги</li>
							<li>проекты</li>
							<li>спецпредложения</li>
							<li>контакты</li>
						</ul>
						<?php
							if (isset($_SESSION['logauth'])) 
							{
								echo '<ul class="log_button" id="log_personal_out">
								<li><img src="img/icon/User.png" id="user_position"></li>
								<li><a href="/OfficePage.php" class="link_style">Добро пожаловать, '.$_SESSION['logauth']->name.'</a></li>
								<li><span class="break_line">|</span></li>
								<li><a href="/LogOut.php" class="link_style">Выход</a></li>
								</ul>';
							}else
							{
								echo '<ul class="log_button" id="log_personal_in">
								<li><img src="img/icon/User.png" id="user_position"></li>
								<li><a href="/AuthForm.php" class="link_style">Вход</a></li>
								<li><span class="break_line">|</span></li>
								<li><a href="/RegistForm.php" class="link_style">Регистрация</a></li>
								</ul>';
							}
						?>
					</div>
					<?php
						if (isset($_SESSION['logauth']))
						{
							echo '<a href="/OrderBuy.php" class="link_style_buy"><div class="orderbuy"><div class="radius_buy"><span id="buy">&#36</span></div></div></a>';
						}else
						{
							echo '<a href="/AuthForm.php" class="link_style_buy"><div class="orderbuy"><div class="radius_buy"><span id="buy">&#36</span></div></div></a>';
						}
					?>
				</div>
			</div>
			<div class="content">
				<div class="position">
					<div class="content_block" id="linestyle">
						<div class="cblock_one">
							<p class="csubtext">Тюнинг-ателье <span class="jcolor">J</span>RC</p>
							<p class="ctext">JRC та компания, которая с лёгкостью превратит вашего<br>
							японского друга, в брутального самурая! Наша компания по своей<br>
							концепции имеет узкую направленность, а именно работает только<br>
							с японскими автомобилями, поэтому мы отлично понимаем в чём<br>
							заключается путь самурая! В нашей команде рабоатют высококлассные<br>
							специалисты каждый из которых стремится внести свою<br>
							лепту в получение радующего глаз и согревающего<br>
							душу итогового результата!</p>
						</div>
						<div class="cblock_one">
							<p class="csubtext" id="csubtext_position">Тюнинг премиум класса</p>
							<ul class="tunelist">
								<li><div class="tunelist_style"><p class="numberlist_position">1.</p></div>Тюнинг мирового уровня от профессионалов</li>
								<li><div class="tunelist_style"><p class="numberlist_position">2.</p></div>Высококачественные комплектующие</li>
								<li><div class="tunelist_style"><p class="numberlist_position">3.</p></div>Доверие, честность, передовой опыт</li>
								<li><div class="tunelist_style"><p class="numberlist_position">4.</p></div>Уникальный стиль</li>
								<li><div class="tunelist_style"><p class="numberlist_position">5.</p></div>Широкий спектр возможностей по тюнингу автомобилей</li>
								<li><div class="tunelist_style"><p class="numberlist_position">6.</p></div>Беспрецедентные возможности для индивидуализации</li>
							</ul>
						</div>
					</div>
					<div class="content_block" id="content_rowstyle">
						<div class="cblock_two" id="content_newsstyle">
							<div class="news_title">
								<p class="title_style">новости</p>
							</div>
							<ul class="news_list">
								<li>07.05.2018: Пошив салона и<br>
								покраска Nissan Laurel C33.<br>
								<span class="read_then">Читать далее➞</span></li>
								<li>10.05.2018: "Злая" Toyota<br>
								Supra JZA80 - начало!<br>
								<span class="read_then">Читать далее➞</span></li>
								<li>19.05.2018: JRC на Московском тюнинг-шоу.<br>
								<span class="read_then">Читать далее➞</span></li>
							</ul>
						</div>
						<div class="cblock_two" id="content_projectstyle">
							<div class="project_title">
								<p class="title_style">наши проекты</p>
							</div>
							<p class="project_button"><span class="text_button_position">смотреть ещё</span></p>
						</div>
						<div class="cblock_two" id="content_stockstyle">
							<div class="stock_title">
								<p class="title_style">акции</p>
							</div>
							<div class="stockspec">
								<p class="stockspec_title">специальное<br>
								предложение</p>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="footer">
				<div class="position">
					<div class="footer_menu">
						<div class="fblock_menu">
							<p class="flogo"><span class="jcolor">j</span>rc<span id="fdomen"><span id="fpoint">&#9632</span>ru</span></p>
							<p class="ftext">Вся представленная на сайте информация,<br>
							касающаяся технических характеристик, наличия на<br> 
							складе, стоимости товаров, носит информационный<br> 
							характер и ни при каких условиях не является<br>
							публичной офертой, определяемой положениями<br>
							Статьи 437(2) Гражданского кодекса РФ</p>
						</div>
						<div class="fblock_menu">
							<p class="fsubtext">Контакты</p>
							<ul class="fcontact_menu">
								<li><img src="img/icon/Geo.png" class="icon_position">Ульяновск, Проспект Авиастроителей, 25</li>
								<li><img src="img/icon/Phone.png" class="icon_position">+7(8422)775-05-35</li>
								<li><img src="img/icon/Email.png" class="icon_position"><span id="femail">info@jrc.ru</span></li>
							</ul>
						</div>
						<div class="fblock_menu">
							<p class="fsubtext" id="fsubtext_position">Меню</p>
							<div class="menu_list">
								<ul class="fcontact_menu hover_style ftext_style">
									<li>новости</li>
									<li>о нас</li>
									<li>услуги</li>
								</ul>
								<ul class="fcontact_menu hover_style ftext_style">
									<li>спецпредложения</li>
									<li>проекты</li>
									<li>контакты</li>
								</ul>
							</div>
						</div>
					</div>
					<p class="copyrite">© 2017-2018. <span id="jrc_style">Japan Ride Customs.</span> Японская тачка, может многое, но заряженная японская тачка, может всё!</p>
				</div>
			</div>
		</div>
	</body>
</html>