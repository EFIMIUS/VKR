<?php
	require "DataBase.php";
	$data = $_POST;
	$user = R::findOne('user', 'login = ?', array($data['login']));
	$admin = R::findOne('admin', 'login = ?', array($data['login']));
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8"/>
		<link rel="stylesheet" type="text/css" href="css/AuthForm.css">
		<link rel="stylesheet" type="text/css" href="css/Fonts.css">
		<title>ВХОД | JAPAN RIDE CUSTOMS</title>
	</head>
	<body>
		<div class="wrapper">
			<a href="/Index.php"><img src="img/BackgroundForm.jpg" class="img_position"></a>
			<div class="authform">
				<?php
					if (isset($data['log_active']))
					{
						if ($user) 
						{
							if (password_verify($data['password'], $user->password))
							{
								$_SESSION['logauth'] = $user;
								?><script>window.location.href = "Index.php"</script><?
							}
							else
							{	
								echo '<p class="error_message">Неверный пароль</p>';
							}
						}
						else if ($admin) 
						{
							if (password_verify($data['password'], $admin->password))
							{
								$_SESSION['logauth'] = $admin;
								?><script>window.location.href = "AdminOrder.php"</script><?
							}
							else
							{
								echo '<p class="error_message">Неверный пароль</p>';
							}
						}
						else
						{
							echo '<p class="error_message">Такого пользователя не существует</p>';
						}	
					}
				?>
				<p class="selectform" id="login">Вход</p>
				<a href="/RegistForm.php" class="link_form_style"><p class="selectform" id="registration">Регистрация</p></a>
				<form action="/AuthForm.php" method="POST">
					<p class="log_position">
						<label class="labelform" id="labellogin">Логин</label>
						<input type="text" name="login" class="inputform">
					</p>
					<p class="log_position">
						<label class="labelform" id="labelpassword">Пароль</label>
						<input type="password" name="password" class="inputform">
					</p>
					<p>
						<input type="checkbox" name="saveme" class="saveme" id="checkbox">
						<label for="checkbox" class="styletext"><span></span>Запомнить меня</label>
						<button class="loginin" type="submit" name="log_active">Войти!</button>
					</p>
					<p class="lostpass">Забыли пароль?</p>
				</form>
			</div>
		</div>
	</body>
</html>
