<?php
	require "DataBase.php";
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8"/>
		<link rel="stylesheet" type="text/css" href="css/AdminPage.css">
		<link rel="stylesheet" type="text/css" href="css/Fonts.css">
		<title>ПАНЕЛЬ УПРАВЛЕНИЯ</title>
	</head>
	<body>
		<div class="wrapper">
			<div class="header">
				<div class="panel_controll">
					<div class="position">
						<p class="title_admin"><span class="panel_simbol">п</span>анель управления</p>
						<ul class="log_button">
							<li><img src="img/icon/User.png" id="user_position"></li>
							<li>Добро пожаловать, <?php echo $_SESSION['logauth']->name?></li>
							<li><span class="break_line">|</span></li>
							<li><a href="/LogOut.php" class="link_style">Выход</a></li>
						</ul>
					</div>
				</div>
			</div>
			<div class="content">
				<div class="position">
					<div class="admin_view">
						<div class="admin_bar">
							<p class="title_menu"><span class="panel_simbol">м</span>еню</p>
							<ul class="admin_bar_style">
								<li><a href="/AdminOrder.php" class="link_style_bar"><p class="button_position" id="orders">Заказы/</p></a></li>
								<li><a href="/AdminUser.php" class="link_style_bar"><p class="button_position">Пользователи/</p></a></li>
							</ul>
						</div>
						<div class="info_bar">
							<table class="table_style table_position">
								<thead>
									<tr>
										<th>#</th>
										<th>Логин</th>
										<th>E-mail</th>
										<th>Имя<br>клиента</th>
										<th>Фамилия<br>клиента</th>
										<th>Номер<br>телефона</th>
										<th>Возможные<br>действия</th>
									</tr>
								</thead>
								<tbody>
										<? $user = R::getAll('SELECT * FROM user');	
										foreach ($user as $user_list) 
										{?>
											<tr>
												<form>
													<td><?= $user_list['id'] ?></td>
													<td><?= $user_list['login'] ?></td>
													<td><?= $user_list['email'] ?></td>
													<td><?= $user_list['name'] ?></td>
													<td><?= $user_list['second_name'] ?></td>
													<td><?= $user_list['phone'] ?></td>
													<td>
														<button class="button_delete active_delete" type="submit" data-user-id="<?= $user_list['id'] ?>">Удалить пользователя</button>
													</td>
												</form>
											</tr>;
										<?}?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			<div class="footer">
				<div class="position">
					<p class="copyrite">© 2017-2018. <span id="jrc_style">Japan Ride Customs.</span> Японская тачка, может многое, но заряженная японская тачка, может всё!</p>
				</div>
			</div>
		</div>
		<script src="js/jquery.js"></script>
		<script src="js/main.js"></script>
	</body>
</html>