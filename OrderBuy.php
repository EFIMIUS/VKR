<?php
	require "DataBase.php";

	$data = $_POST;
	$status = R::load('status', 1);
	if (isset($data['click_order']))
	{
		$order = R::dispense('ordering');
		$order->user_id = $_SESSION['logauth']->id;
		$order->mark = $data['mark'];
		$order->model = $data['model'];
		$order->ago = $data['ago'];
		$order->service = $data['service'];
		$order->info = $data['info'];
		$order->date_ord = date("d-m-Y");
		$order->status_id = $status->id;
		R::store($order);
		header('Location: OfficePage.php');
		exit();
	}
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>ОФОРМИТЬ ЗАКАЗ | JAPAN RIDE CUSTOMS</title>
		<link rel="stylesheet" type="text/css" href="css/OrderBuy.css">
		<link rel="stylesheet" type="text/css" href="css/Fonts.css">
		<link rel="stylesheet" type="text/css" href="css/Header.css">
		<link rel="stylesheet" type="text/css" href="css/Footer.css">
	</head>
	<body>
		<div class="wrapper">
			<div class="header">
				<div class="navigation">
					<div class="position">
						<a href="/Index.php" class="link_decor"><p class="logo"><span class="jcolor">j</span>rc<span id="domen"><span id="point">&#9632</span>ru</span></p></a>
						<ul class="nav_button">
							<li>новости</li>
							<li>о нас</li>
							<li>услуги</li>
							<li>проекты</li>
							<li>спецпредложения</li>
							<li>контакты</li>
						</ul>
						<?php
							if (isset($_SESSION['logauth'])) 
							{
								echo '<ul class="log_button" id="log_personal_out">
								<li><img src="img/icon/User.png" id="user_position"></li>
								<li><a href="/OfficePage.php" class="link_style">Добро пожаловать, '.$_SESSION['logauth']->name.'</a></li>
								<li><span class="break_line">|</span></li>
								<li><a href="/LogOut.php" class="link_style">Выход</a></li>
								</ul>';
							}else
							{
								echo '<ul class="log_button" id="log_personal_in">
								<li><img src="img/icon/User.png" id="user_position"></li>
								<li><a href="/AuthForm.php" class="link_style">Вход</a></li>
								<li><span class="break_line">|</span></li>
								<li><a href="/RegistForm.php" class="link_style">Регистрация</a></li>
								</ul>';
							}
						?>
					</div>
					<a href="/OrderBuy.php" class="link_style_buy"><div class="orderbuy"><div class="radius_buy"><span id="buy">&#36</span></div></div></a>
				</div>
			</div>
			<div class="content">
				<div class="position">
					<p class="errors_order">Заполнены не все поля!</p>
					<p class="title_page"><span class="title_startsimbol">О</span>формление заказа</p>
					<form class="form_style" id="form_validation" method="POST">
						<p class="ordering_form">
							<label class="label_style">Марка автомобиля</label>
							<select size="1" name="mark" class="select_style" id="mark_avto">
								<option selected value="">Марка автомобиля</option>
								<option value="Toyota">Toyota</option>
								<option value="Honda">Honda</option>
								<option value="Nissan">Nissan</option>
							</select>
						</p>
						<p class="ordering_form">
							<label class="label_style">Модель автомобиля</label>
							<select size="1" name="model" class="select_style"  id="model_avto">
								<option selected value="">Модель автомобиля</option>
								<option class="toyota_model" value="Corolla" >Corolla</option>
								<option class="toyota_model" value="Chaser" >Chaser</option>
								<option class="toyota_model" value="MarkII" >MarkII</option>
								<option class="toyota_model" value="Cresta" >Cresta</option>
								<option class="toyota_model" value="Crown" >Crown</option>
								<option class="toyota_model" value="Levin" >Levin</option>
								<option class="toyota_model" value="Supra" >Supra</option>
								<option class="toyota_model" value="Rav4" >Rav4</option>
								<option class="toyota_model" value="Sprinter Carib" >Sprinter Carib</option>
								<option class="toyota_model" value="Sprinter Trueno" >Sprinter Trueno</option>
								<option class="honda_model" value="Civic" >Civic</option>
								<option class="honda_model" value="Accord" >Accord</option>
								<option class="honda_model" value="Inspire" >Inspire</option>
								<option class="honda_model" value="Legend" >Legend</option>
								<option class="honda_model" value="HR-V" >HR-V</option>
								<option class="honda_model" value="NSX" >NSX</option>
								<option class="honda_model" value="S2000" >S2000</option>
								<option class="honda_model" value="Prelude" >Prelude</option>
								<option class="honda_model" value="CR-Z" >CR-Z</option>
								<option class="honda_model" value="CR-X" >CR-X</option>
								<option class="nissan_model" value="350Z" >350Z</option>
								<option class="nissan_model" value="240SX" >240SX</option>
								<option class="nissan_model" value="Skyline GT-R" >Skyline GT-R</option>
								<option class="nissan_model" value="Silvia" >Silvia</option>
								<option class="nissan_model" value="Fairlady Z" >Fairlady Z</option>
								<option class="nissan_model" value="Bluebird" >Bluebird</option>
								<option class="nissan_model" value="Cedric" >Cedric</option>
								<option class="nissan_model" value="Laurel" >Laurel</option>
								<option class="nissan_model" value="Gloria" >Gloria</option>
								<option class="nissan_model" value="GT-R" >GT-R</option>
							</select>
						</p>
						<p class="ordering_form">
							<label class="label_style">Год выпуска автомобиля</label>
							<select size="1" name="ago" class="select_style" id="ago_avto">
								<option selected value="">Год выпуска</option>
								<option class="ago_avto" value="1980-1989">1980-1989</option>
								<option class="ago_avto" value="1990-1999">1990-1999</option>
								<option class="ago_avto" value="2000-2009">2000-2009</option>
							</select>
						</p>
						<p class="ordering_form">
							<label class="label_style">Вид работы</label>
							<select size="1" name="service" class="select_style" id="service">
								<option selected value="">Выбор работы</option>
								<option class="service" value="Установка аудио компанентов">Установка аудио компанентов</option>
								<option class="service" value="Пошивка салона">Пошивка салона</option>
								<option class="service" value="Шумо и виброизоляция автомобиля">Шумо и виброизоляция автомобиля</option>
								<option class="service" value="Ремонт и покраска кузова">Ремонт и покраска кузова</option>
								<option class="service" value="Установка обвесов">Установка обвесов</option>
								<option class="service" value="Аэрография">Аэрография</option>
								<option class="service" value="Слесарные работы">Слесарные работы</option>
								<option class="service" value="Тюнинг двигателя и других агрегатов">Тюнинг двигателя и других агрегатов</option>
								<option class="service" value="СВАП двигателя и кпп">СВАП двигателя и кпп</option>
							</select>
						</p>
						<p class="ordering_form">
							<label class="label_style">Дополнительные сведения к заказываемой работе</label>
							<textarea class="textarea_style" name="info" id="dop_info"></textarea>
						</p>
						<button class="clickbuy_style click_order" type="submit" name="click_order">Оформить!</button>
					</form>
				</div>
			</div>
			<div class="footer">
				<div class="position">
					<div class="footer_menu">
						<div class="fblock_menu">
							<p class="flogo"><span class="jcolor">j</span>rc<span id="fdomen"><span id="fpoint">&#9632</span>ru</span></p>
							<p class="ftext">Вся представленная на сайте информация,<br>
							касающаяся технических характеристик, наличия на<br> 
							складе, стоимости товаров, носит информационный<br> 
							характер и ни при каких условиях не является<br>
							публичной офертой, определяемой положениями<br>
							Статьи 437(2) Гражданского кодекса РФ</p>
						</div>
						<div class="fblock_menu">
							<p class="fsubtext">Контакты</p>
							<ul class="fcontact_menu">
								<li><img src="img/icon/Geo.png" class="icon_position">Ульяновск, Проспект Авиастроителей, 25</li>
								<li><img src="img/icon/Phone.png" class="icon_position">+7(8422)775-05-35</li>
								<li><img src="img/icon/Email.png" class="icon_position"><span id="femail">info@jrc.ru</span></li>
							</ul>
						</div>
						<div class="fblock_menu">
							<p class="fsubtext" id="fsubtext_position">Меню</p>
							<div class="menu_list">
								<ul class="fcontact_menu hover_style ftext_style">
									<li>новости</li>
									<li>о нас</li>
									<li>услуги</li>
								</ul>
								<ul class="fcontact_menu hover_style ftext_style">
									<li>спецпредложения</li>
									<li>проекты</li>
									<li>контакты</li>
								</ul>
							</div>
						</div>
					</div>
					<p class="copyrite">© 2017-2018. <span id="jrc_style">Japan Ride Customs.</span> Японская тачка, может многое, но заряженная японская тачка, может всё!</p>
				</div>
			</div>
		</div>
		<script src="js/jquery.js"></script>
		<script src="js/main.js"></script>	
	</body>
</html>