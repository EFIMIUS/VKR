<?php
	require "DataBase.php";

?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8"/>
		<link rel="stylesheet" type="text/css" href="css/AdminPage.css">
		<link rel="stylesheet" type="text/css" href="css/Fonts.css">
		<title>ПАНЕЛЬ УПРАВЛЕНИЯ</title>
	</head>
	<body>
		<div class="wrapper">
			<div class="header">
				<div class="panel_controll">
					<div class="position">
						<p class="title_admin"><span class="panel_simbol">п</span>анель управления</p>
						<ul class="log_button">
							<li><img src="img/icon/User.png" id="user_position"></li>
							<li>Добро пожаловать, <?php echo $_SESSION['logauth']->name?></li>
							<li><span class="break_line">|</span></li>
							<li><a href="/LogOut.php" class="link_style">Выход</a></li>
						</ul>
					</div>
				</div>
			</div>
			<div class="content">
				<div class="position">
					<div class="admin_view">
						<div class="admin_bar">
							<p class="title_menu"><span class="panel_simbol">м</span>еню</p>
							<ul class="admin_bar_style">
								<li><a href="/AdminOrder.php" class="link_style_bar"><p class="button_position" id="orders">Заказы/</p></a></li>
								<li><a href="/AdminUser.php" class="link_style_bar"><p class="button_position">Пользователи/</p></a></li>
							</ul>
						</div>
						<div class="info_bar">
							<table class="table_style">
								<thead>
									<tr>
										<th>Имя<br>клиента</th>
										<th>Фамилия<br>клиента</th>
										<th>E-mail</th>
										<th>Дата<br>заказа</th>
										<th>Наименование<br>услуги</th>
										<th>Марка<br>автомобиля</th>
										<th>Модель<br>автомобиля</th>
										<th>Год<br>выпуска</th>
										<th>Дополнительные<br>сведения</th>
										<th>Статус<br>выполнеиня</th>
									</tr>
								</thead>
								<tbody>
										<? $user = R::getAll('SELECT id, name, second_name, email FROM user WHERE id IN (SELECT user_id FROM ordering)');
										$order = R::getAll('SELECT * FROM ordering');	
										$new_user = array();
										foreach ($user as $one_user) {
											$new_user[$one_user['id']] = $one_user;
										}
										foreach ($order as $checklist) 
										{?>
											<tr>
												<td><?= $new_user[$checklist['user_id']]['name'] ?></td>
												<td><?= $new_user[$checklist['user_id']]['second_name'] ?></td>
												<td><?= $new_user[$checklist['user_id']]['email'] ?></td>
												<td><?= $checklist['date_ord'] ?></td>
												<td><?= $checklist['service'] ?></td>
												<td><?= $checklist['mark'] ?></td>
												<td><?= $checklist['model'] ?></td>
												<td><?= $checklist['ago'] ?></td>
												<td><?= $checklist['info'] ?></td>
												<form class="status_form">
													<td>
														<select class="status_style status_active" data-order-id="<?= $checklist['id'] ?>">
														<? $status = R::getAll('SELECT id, name FROM status');
														foreach ($status as $status_option)
														{?>
															<option value="<?= $status_option['id'] ?>" <? if($checklist['status_id'] == $status_option['id']) {?>selected<?}?>><?= $status_option['name'] ?></option>
														<?}?>
														</select>
													</td>
												</form>
											</tr>
										<?}?>
								</tbody> 
							</table>
						</div>
					</div>
				</div>
			</div>
			<div class="footer">
				<div class="position">
					<p class="copyrite">© 2017-2018. <span id="jrc_style">Japan Ride Customs.</span> Японская тачка, может многое, но заряженная японская тачка, может всё!</p>
				</div>
			</div>
		</div>
		<script src="js/jquery.js"></script>
		<script src="js/main.js"></script>	
	</body>
</html>