<?php
	require "DataBase.php";
	$data = $_POST;
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8"/>
		<link rel="stylesheet" type="text/css" href="css/RegistForm.css">
		<link rel="stylesheet" type="text/css" href="css/Fonts.css">
		<title>РЕГИСТРАЦИЯ | JAPAN RIDE CUSTOMS</title>
	</head>
	<body>
		<div class="wrapper">
			<a href="/Index.php"><img src="img/BackgroundForm.jpg" class="img_position"></a>
			<div class="registform">
				<a href="/AuthForm.php" class="link_form_style"><p class="selectform" id="login">Вход</p></a>
				<p class="selectform" id="registration">Регистрация</p>
				<?php
					if (isset($data['reg_active']))
					{
						$errors = array();
						if (trim($data['login']) == '')
						{
							$errors[] = 'Введите логин';
						}
						if (trim($data['email']) == '')
						{
							$errors[] = 'Введите email';
						}
						if (trim($data['name']) == '')
						{
							$errors[] = 'Введите имя';
						}
						if (trim($data['second_name']) == '')
						{
							$errors[] = 'Введите фамилию';
						}
						if (trim($data['phone']) == '')
						{
							$errors[] = 'Введите номер телефона';
						}
						if ($data['password'] == '')
						{
							$errors[] = 'Введите пароль';
						}
						if ($data['password_repeat'] != $data['password'])
						{
							$errors[] = 'Пароли не совпадают';
						}
						$name_check = '/^[a-zёа-я]+$/ui';
						$email_check = '/^([a-zA-Z0-9_.-])+@([a-zA-Z0-9_.-])+\.([a-zA-Z])+([a-zA-Z])+/';
						$phone_check = '/^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$/';
						if(empty(preg_match($name_check, $data['name'])))
						{
							$errors[] = 'Имя должно стостоять только из букв';
						}
						if(empty(preg_match($email_check, $data['email'])))
						{
							$errors[] = 'Неверный почтовый ящик';
						}
						if(empty(preg_match($phone_check, $data['phone'])))
						{
							$errors[] = 'Введиет номер в формате +7(...)- или 8(...)-';
						}
						if (empty($errors)) 
						{
							$user = R::dispense('user');
							$user->login = $data['login'];
							$user->email = $data['email'];
							$user->name = $data['name'];
							$user->second_name = $data['second_name'];
							$user->phone = $data['phone'];
							$user->password = password_hash($data['password'], PASSWORD_DEFAULT);
							R::store($user);
							?><script>window.location.href = "Index.php"</script><?
							
						}
						else
						{
							echo '<p style="position: absolute;
								top: 25px;
								font-family: RobotoCondensedBold;
								font-size: 14pt;
								color: #911e1e;
								z-index: 1000;">'.array_shift($errors).'</p>';
						}
					}
				?>
				<form action="/RegistForm.php" method="POST">

					<p class="input_positon">
						<label class="labelform">Логин</label>
						<input type="text" name="login" class="inputform">
					</p>
					<p class="input_positon">
						<label class="labelform">E-mail</label>
						<input type="text" name="email" class="inputform">
					</p>
					<p class="input_positon">
						<label class="labelform">Имя</label>
						<input type="text" name="name" class="inputform">
					</p>
					<p class="input_positon">
						<label class="labelform">Фамилия</label>
						<input type="text" name="second_name" class="inputform">
					</p>
					<p class="input_positon">
						<label class="labelform">Номер телефона</label>
						<input type="text" name="phone" class="inputform">
					</p>
					<p class="input_positon">
						<label class="labelform">Пароль</label>
						<input type="password" name="password" class="inputform">
					</p>
					<p class="input_positon">
						<label class="labelform">Повторить пароль</label>
						<input type="password" name="password_repeat" class="inputform">
					</p>
					<button class="registration" type="submit" name="reg_active">Зарегистрировать!</button>
				</form>
			</div>
		</div>
	</body>
</html>